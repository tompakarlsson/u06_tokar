'module containing CRUD functions'

from uuid import UUID, uuid4

from sqlmodel import Session, select

# from ..database import engine
from ..models import Prices, Products, Inventory, Stores, Sales, SoldProductsBase, SoldProducts, InventoryBase


def get_price(session: Session, article_nr: int):
    """
    Get price for a product
    :param session:
    :param article_nr:
    :return:
    """
    statement = select(Prices.price).where(Prices.product == Products.id).where(Products.article_nr == article_nr)
    results = session.exec(statement)
    price = results.fetchall()[0]
    print(price)
    return price


def get_products_from_inventory(session: Session, store_id: str) -> list[str]:
    """
    Get products in Inventory for a specific store

    Usage:
    get_inventory(uuid4)

    :param session: Session
    :param store_id: id for store to check inventory of
    :return: list: all products in a specific stores inventory
    """
    statement = select(Inventory.product).where(Inventory.store == store_id)
    results = session.exec(statement)
    prod_list: list[str] = results.all()
    return prod_list


def get_store_id(session: Session, store_name: str) -> str:
    """
    Get id for a store specified by its name

    Usage:
    get_store_id('Djurjouren')

    :param session: Session
    :param store_name: str: Name of store to get id for
    :return: str: id of a specific store
    """
    store_name: str = store_name.title()
    statement = select(Stores.id).where(Stores.name == store_name)
    results = session.exec(statement)
    store_id: str = results.one()
    return store_id


def add_sale(session: Session, store_id: str):
    """
    Adding sale to Sales table
    :param session: Session
    :param store_id: str
    :return:
    """
    new_uuid_sales: str = str(uuid4())
    session.add(Sales(id=new_uuid_sales, store=store_id))
    return new_uuid_sales


def get_product_id(session: Session, article_nr: int) -> str:
    """
    Getting product id from Products table
    :param session: Session
    :param article_nr: int
    :return:
    """
    statement = select(Products.id).where(Products.article_nr == article_nr)
    results = session.exec(statement)
    prod_id: str = results.first()
    return prod_id


def add_sold_products(session: Session, sold_products: SoldProductsBase) -> str:
    """
    Adding sold products to database. creating UUID
    :param session: Session
    :param sold_products: SoldProductsBase
    :return: str
    """
    new_uuid_sold_products = str(uuid4())
    session.add(SoldProducts(id=new_uuid_sold_products, **sold_products.dict()))
    return new_uuid_sold_products


def get_inventory(session: Session, store_id: str, prod_id: str) -> Inventory:
    """
    Read inventory by Inventory.store and Inventory.product
    :param session: Session
    :param store_id: str
    :param prod_id: str
    :return: Inventory
    """
    statement = select(Inventory).where(Inventory.store == store_id).where(Inventory.product == prod_id)
    results = session.exec(statement)
    return results.one()


def update_inventory(session: Session, quantity: int, inventory: Inventory):
    """
    Update Inventory
    :param session: Session
    :param quantity: int
    :param inventory: Inventory
    :return:
    """
    inventory.quantity += quantity
    session.add(inventory)
    session.commit()


def create_inventory(session: Session, inventory: InventoryBase):
    """
    Create inventory
    :param session:
    :param inventory:
    :return:
    """
    new_uuid_inv: UUID = uuid4()
    session.add(Inventory(id=str(new_uuid_inv), **inventory.dict()))
    return new_uuid_inv
