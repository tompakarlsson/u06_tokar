"""Observer
Watches a specific directory.
When a file is created in that directory actions specific actions are performed.
Note: All output is logged to stdout
"""
from decimal import getcontext
import logging
import logging.config
from os import listdir
from shutil import move
import time

from pandas import read_excel, DataFrame

from sqlmodel import Session
from sqlmodel.sql.expression import Select, SelectOfScalar

from watchdog.observers import Observer
from watchdog.events import PatternMatchingEventHandler

from ..config import settings
from ..database import engine
from ..models import Inventory, SoldProductsBase, InventoryBase, Order
from .crud import get_store_id, get_inventory, add_sale, get_products_from_inventory, get_product_id, \
    add_sold_products, update_inventory, create_inventory
from .create_invoice_documentation import data_to_csv

# Workaround for SAWarcning in SQLModel (discussed here: https://github.com/tiangolo/sqlmodel/issues/189)
SelectOfScalar.inherit_cache = True
Select.inherit_cache = True

# Set number of decimals in type Decimal
getcontext().prec = 2

# Set basic configurations for logging
logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger(__name__)


class OnMyWatch:  # pragma: no cover  # pylint: disable=too-few-public-methods
    """
    Watchdog class
    """

    def __init__(self):
        self.observer = Observer()

    def run(self):
        """Watchdog runner"""
        event_handler = Handler()
        self.observer.schedule(event_handler, settings.source_path, recursive=False)
        self.observer.start()
        try:
            while True:
                time.sleep(5)
        except KeyboardInterrupt:
            self.observer.stop()
            logging.info(
                """Observer stopped
                ----------------""")
        self.observer.join()


class Handler(PatternMatchingEventHandler):  # pragma: no cover
    """
    Event handler for watchdog. Describes what to do when an excel file is created in our monitored directory.
    """
    patterns = ('*.xls', '*.xlsx')

    @staticmethod
    def on_created(event):  # pylint: disable=inconsistent-return-statements
        """
        Describes actions taken when watchdog.events.FileSystemEventHandler observes creation of an object
        :param event: Event representing file/directory creation
        :return:
        """

        if event.event_type == 'created':
            orders: list[Order] = find_and_load_orders()
            with Session(engine) as session:
                for order in orders:
                    # Export all collected data to .csv in csv directory
                    data_to_csv(session=session, order=order, dest_path=settings.dest_path_csv)

        elif event.is_directory:
            return None


def find_and_load_orders(sourcepath=settings.source_path, destpath=settings.dest_path_orders) -> list[Order]:
    """
    Find excel files and call functions to parse excel, add data to database and move parsed files to archive
    :return: list[Order]
    """
    files: list = listdir(sourcepath)
    orders = []
    for filename in files:
        if filename.endswith('.xlsx') or filename.endswith('.xls'):
            order: Order = parse_order(filename=f'{sourcepath}{filename}')
            # try:
            #     order: Order = parse_order(filename=f'{sourcepath}{filename}')
            # except TypeError:
            #     logging.error(
            #         f"""ERROR
            #         Type error from {filename}""",
            #         exc_info=True)
            #     break
            if order:
                # Add sale to database
                with Session(engine) as session:
                    handle_sale(store_name=order.storename, products=order.prods, quantities=order.qty, session=session)

                # Move parsed files to archive directory after parsing
                move(f'{sourcepath}{filename}', f'{destpath}{filename}')
                logging.info(f'{filename} moved to {destpath}')

                orders.append(order)

    return orders


def parse_order(filename: str, sourcepath=settings.source_path) -> Order | None:
    """
    Load and parse a .xlsx or .xls file
    :param sourcepath:
    :param filename: str
    :return: Order | None
    """
    try:
        excel_data: DataFrame = read_excel(filename)
    except ValueError:
        move(filename, f'{sourcepath}corrupt/{filename.rsplit("/")[-1]}')
        logging.error(
            f"""ERROR
            Excel file format cannot be determined. Moving {filename} to directory 'corrupt'""",
            exc_info=True)
        return None
    try:
        order = Order(
            order_name=filename.rsplit('/')[-1],
            storename=excel_data.at[1, 'Order form'],
            storeaddress=excel_data.at[1, 'Giaco’s pretty pets'],
            storezip=str(int(excel_data.at[2, 'Giaco’s pretty pets'])),
            storecity=excel_data.at[3, 'Giaco’s pretty pets'],
            prods=[
                excel_data.at[i, 'Order form']
                for i in range(6, len(excel_data.index))
            ],
            qty=[
                excel_data.at[i, 'Giaco’s pretty pets']
                for i in range(6, len(excel_data.index))
            ],
        )
        logging.info(f'Found order from {order.storename} containing {len(order.prods)} products ({filename})')
        return order
    except KeyError:
        move(filename, f'{sourcepath}corrupt/{filename.rsplit("/")[-1]}')
        logging.error(
            f"""ERROR
            Order format can not be recogniced. Moving {filename} to directory 'corrupt'""",
            exc_info=True)
        return None


def handle_sale(session: Session, store_name: str, products: list[int], quantities: list[int]):
    """
    Handles database crud
    Uses functions from crud.py:
        get_store_id(store_name=store_name) to get store_id from store_name
        add_sale(session=session, store_id=store_id) to add row in Sales
        get_product_id(session=session, article_nr=article_nr) to get product_id from article_nr
        add_sold_products(session=session, sold_products=sold_product) to add row in SoldProducts
        get_products_from_inventory(store_id) to get a list of products by store_id in Inventory
        get_inventory(session=session, prod_id=prod_id, store_id=store_id) to get Inventory for a store
        update_inventory(session=session, inventory=inventory, quantity=quantiy) to update row in Inventory
        create_inventory(session=session, inventory=new_inventory) to create a new row in Inventory

    Usage:
    handle_sale('Djurjouren', ['Kattmat', 'Kattklonare'], [10, 15])

    :param session: Session
    :param store_name: str: Name of store
    :param products: list[int]: ordered products
    :param quantities: list[int]: ordered quantity of products
    :return: None
    """
    store_id = get_store_id(session=session, store_name=store_name)
    sale_id = add_sale(session=session, store_id=store_id)

    for article_nr, quantity in zip(products, quantities):
        prod_id = get_product_id(session=session, article_nr=article_nr)
        sold_product = SoldProductsBase(
            product=prod_id,
            sale=sale_id,
            quantity=quantity)
        add_sold_products(session=session, sold_products=sold_product)
        if prod_id in get_products_from_inventory(session=session, store_id=store_id):
            inventory: Inventory = get_inventory(session=session, prod_id=prod_id, store_id=store_id)
            update_inventory(session=session, inventory=inventory, quantity=quantity)
        else:
            new_inventory = InventoryBase(product=prod_id, quantity=quantity, store=store_id)
            create_inventory(session=session, inventory=new_inventory)
    logging.info('Order written to database')


if __name__ == "__main__":  # pragma: no cover
    logging.info('Observer started')
    watch = OnMyWatch()
    watch.run()
