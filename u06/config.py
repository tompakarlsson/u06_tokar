from pathlib import Path
from typing import Optional

from pydantic import BaseSettings

THIS_PACKAGE = Path(__file__).parent
PACKAGE_ROOT: Path = THIS_PACKAGE.parent
ENV_FILE: Path = PACKAGE_ROOT / '.env'


class Settings(BaseSettings):
    """Path configurations for Observer"""

    source_path: Optional[str]
    dest_path_orders: Optional[str]
    dest_path_csv: Optional[str]
    db_path: Optional[str]

    class Config:
        env_file = str(ENV_FILE)


settings = Settings()
