from sqlmodel import Session, select
from sqlmodel.sql.expression import Select, SelectOfScalar

from u06.models import Sales, SoldProductsBase, SoldProducts, Inventory, Products, Stores, Prices
from u06.src.crud import add_sale, add_sold_products, create_inventory, get_products_from_inventory, get_inventory, \
    update_inventory, get_product_id, get_store_id, get_price

# Workaround for SAWarcning in SQLModel (discussed here: https://github.com/tiangolo/sqlmodel/issues/189)
SelectOfScalar.inherit_cache = True
Select.inherit_cache = True


def test_add_sale(session: Session):  #
    # GIVEN an empty database session and a store id
    store_id = '676df1a1-f1d1-4ac5-9ee3-c58dfe820927'

    # WHEN running add_sale and reading from db table Sales
    sale_id = add_sale(session=session, store_id=store_id)

    statement = select(Sales).where(Sales.id == sale_id)
    sale = session.exec(statement).one()

    # THEN actual sale is equal to expected sale
    assert sale.store == store_id


def test_add_sold_products(session: Session):
    # GIVEN an empty database session and an instance of SoldProductsBase
    sold_product = SoldProductsBase(
        product='19e67404-6e35-45b7-8d6f-e5bc5b79c453',
        quantity=12,
        sale='b1fea07b-3e59-444d-9f16-c0d73b84ee80'
    )

    # WHEN running add_sold_products and reading from db table SoldProducts
    add_sold_products(session=session, sold_products=sold_product)
    statement = select(SoldProducts).where(SoldProducts.sale == sold_product.sale)
    result = session.exec(statement).one()

    # THEN actual sold products is equal to expected sold products
    assert [result.sale,
            result.quantity,
            result.product
            ] == [
        sold_product.sale,
        sold_product.quantity,
        sold_product.product]


def test_create_inventory(new_inventory, session: Session):
    # GIVEN an inventory (in fixture new_inventory)

    # WHEN running create_inventory and reading from db table Inventory
    create_inventory(session=session, inventory=new_inventory)
    statement = select(Inventory).where(Inventory.product == new_inventory.product)
    result = session.exec(statement).one()

    # THEN actual inventory is equal to expected inventory
    assert [result.product,
            result.quantity,
            result.store
            ] == [
        new_inventory.product,
        new_inventory.quantity,
        new_inventory.store]


def test_get_products_from_inventory(new_inventory, session: Session):
    # GIVEN an inventory in db table Inventory
    create_inventory(session=session, inventory=new_inventory)

    # WHEN running get_products_from_inventory
    product_from_inventory = get_products_from_inventory(session=session, store_id=new_inventory.store)

    # THEN actual product is equal to expected product
    assert product_from_inventory[0] == new_inventory.product


def test_get_inventory(new_inventory, session: Session):
    # GIVEN an inventory in db table Inventory
    create_inventory(session=session, inventory=new_inventory)

    # WHEN running get_inventory
    inventory: Inventory = get_inventory(session=session, prod_id=new_inventory.product, store_id=new_inventory.store)

    # THEN actual inventory is equal to expected inventory
    assert [inventory.product,
            inventory.quantity,
            inventory.store
            ] == [
        new_inventory.product,
        new_inventory.quantity,
        new_inventory.store]


def test_update_inventory(new_inventory, session: Session):
    # GIVEN an inventory in db table Inventory an instance of Inventory and a quantity
    create_inventory(session=session, inventory=new_inventory)
    inventory: Inventory = get_inventory(session=session, prod_id=new_inventory.product, store_id=new_inventory.store)
    quantity = 12

    # WHEN running update_inventory and reading Inventory afterwards
    update_inventory(session=session, inventory=inventory, quantity=quantity)
    statement = select(Inventory).where(Inventory.store == new_inventory.store)
    result = session.exec(statement).one()

    # THEN actual quantity is equal to expected quantity
    assert result.quantity == new_inventory.quantity + quantity


def test_get_product_id(session: Session):
    # GIVEN an article_nr and a product in db table Products
    article_nr = 2047
    session.add(Products(id='19e67404-6e35-45b7-8d6f-e5bc5b79c453',
                         article_nr=article_nr,
                         name='Kattklonare',
                         description='hoppsan'))

    # WHEN running get_product_id
    product_id = get_product_id(session=session, article_nr=article_nr)

    # THEN actual product id is equal to expected product id
    assert product_id == '19e67404-6e35-45b7-8d6f-e5bc5b79c453'


def test_get_store_id(session: Session):
    # GIVEN a store id and a store in db table Stores
    existing_store_id = '75040436-56de-401b-8919-8d0063ac9dd7'
    session.add(Stores(id=existing_store_id, name='Djuristen'))

    # WHEN running get_store_id
    store_id = get_store_id(session=session, store_name='Djuristen')

    # THEN actual store id is equal to expected store id
    assert store_id == existing_store_id


def test_get_price(session: Session):
    # GIVEN a price and a product id and a product in db table Products
    product_price = '55900'
    article_nr = 2047
    session.add(Prices(id='d23677ed-d670-4cb6-a44f-43ed8fb33a4e',
                       product='19e67404-6e35-45b7-8d6f-e5bc5b79c453',
                       price=product_price))
    session.add(Products(id='19e67404-6e35-45b7-8d6f-e5bc5b79c453',
                         article_nr=2047,
                         name='Kattklonare',
                         description='hoppsan'))

    # WHEN running get_price
    price = get_price(session=session, article_nr=article_nr)

    # THEN actual price is equal to expected price
    assert price == product_price
