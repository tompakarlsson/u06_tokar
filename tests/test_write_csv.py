import csv

from sqlmodel import Session
from sqlmodel.sql.expression import Select, SelectOfScalar

from u06.src.create_invoice_documentation import data_to_csv

# Workaround for SAWarcning in SQLModel (discussed here: https://github.com/tiangolo/sqlmodel/issues/189)
SelectOfScalar.inherit_cache = True
Select.inherit_cache = True


def test_data_to_csv(mocker, order, tmpdir, session: Session):
    # GIVEN
    path_to_test_csv = tmpdir.mkdir('test_csv')

    mocker.patch('u06.src.crud.get_price', return_value=459)

    # WHEN
    data_to_csv(session=session, order=order, dest_path=f'{path_to_test_csv}/')

    # THEN
    assert len(tmpdir.listdir()) == 1
    with open(f'{path_to_test_csv}/{order.order_name.rstrip(".xls")}.csv', newline='') as csvfile:
        csv_content = csv.reader(csvfile, delimiter=',')
        content = list(csv_content)
    assert content[1][-2] == '459'
