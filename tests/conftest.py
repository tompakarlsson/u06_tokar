from pathlib import Path
import pandas as pd
import pytest  #
from sqlmodel import Session, SQLModel, create_engine
from sqlmodel.pool import StaticPool

from u06.models import InventoryBase, Order


# @pytest.fixture
# def order_1_xlsx() -> str:
#     """"""
#
#     file_path = 'tests/fixtures/testorder.xlsx'
#     file = Path(file_path)
#     print('hej')
#     reading=pd.read_excel(file.absolute())
#     print('hje')
#     print(reading)
#     return pd.read_excel(file.absolute())


@pytest.fixture(name='session')  #
def session_fixture():  #
    engine = create_engine(
        "sqlite://", connect_args={'check_same_thread': False}, poolclass=StaticPool
    )
    SQLModel.metadata.create_all(engine)
    with Session(engine) as session:
        yield session  #


@pytest.fixture(name='new_inventory')
def inventory_fixture():
    new_inventory = InventoryBase(
        product='19e67404-6e35-45b7-8d6f-e5bc5b79c453',
        quantity=12,
        store='a04bb312-9738-4db2-a7a5-ed6be9938afd'
    )
    return new_inventory


@pytest.fixture(name='order')
def order_fixture():
    order = Order(storename='Djuristen',
                  storeaddress='Skånegatan 420',
                  storezip='54321',
                  storecity='Falun',
                  prods=[511],
                  qty=[1],
                  order_name='order#3.xls')
    return order
